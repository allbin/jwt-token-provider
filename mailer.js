"use strict";
/**
   This file is part of jwt-token-provider.

   jwt-token-provider is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   jwt-token-provider is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with jwt-token-provider.  If not, see <http://www.gnu.org/licenses/>.
*/
module.exports = function(config) {
    var nodemailer = require('nodemailer');
    var fs = require('fs');

    var transporter = nodemailer.createTransport(config.transport);
    transporter.verify(function(error, success) {
        if (error) {
            console.log("Mailer Transport error: ", error);
            process.exit(2);
        } else {
            console.log('Server is ready to take our messages');
        }
    });

    function replaceTemplateParameters(tpl, vars) {
        return tpl.replace(/\{\{(.*?)\}\}/g, function(a,b) {
            return vars[b];
        });
    }

    // Cache templates
    Object.keys(config.emails.lang).filter(function(ln) {
        Object.keys(config.emails.lang[ln]).filter(function(k) {
            ["html", "text"].filter(function(typ) {
                try {
                    config.emails.lang[ln][k][typ] = fs.readFileSync(config.emails.lang[ln][k][typ]).toString();
                } catch(e) {
                    console.error('Failed to load text-template file %s ', typ, config.emails.lang[ln][k][typ]);
                    process.exit(3);
                }
            });
        });
    });

    /*
     * var mailOptions = {
     *  from: '"Fred Foo 👥" <foo@blurdybloop.com>', // sender address 
     *  to: 'bar@blurdybloop.com, baz@blurdybloop.com', // list of receivers 
     *  subject: 'Hello ✔', // Subject line 
     *  text: 'Hello world 🐴', // plaintext body 
     *  html: '<b>Hello world 🐴</b>' // html body 
     * };
     */
    this.sendMail = function( mailOptions, templateParameters) {
        var opts = {
            from: mailOptions.from,
            to: mailOptions.to,
            subject: mailOptions.subject,
            text: replaceTemplateParameters(mailOptions.text, templateParameters),
            html: replaceTemplateParameters(mailOptions.html, templateParameters)
        };

        return new Promise(function(resolve, reject) {
            try {
                transporter.sendMail(opts, function(err, info){
                    if(err) {
                        return reject(err);
                    }
                    resolve(info.response);
                });
            } catch(e) {
                reject(e);
            }
        });
    };

    this.sendTemplateTo = function(templateName, to, lang, templateParameters) {
        var tpl = config.emails.lang[lang][templateName];
        if(!tpl) {
            throw new Error('No such template ' + templateName + ' for language: ' + lang);
        }
        var opts = {from: config.emails.from,
                    to: to,
                    subject: tpl.subject,
                    text: tpl.text,
                    html: tpl.html};
        return this.sendMail(opts, templateParameters);
    }
    
    return this;
};
