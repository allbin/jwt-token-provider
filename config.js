/**
   This file is part of jwt-token-provider.

   jwt-token-provider is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   jwt-token-provider is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with jwt-token-provider.  If not, see <http://www.gnu.org/licenses/>.
*/
var fs = require('fs');
module.exports = {
    version: JSON.parse(fs.readFileSync('./package.json')).version,
    debug: process.env.DEBUG || false,

    db: {
        mongo: {
            url: process.env.MONGO_URL || "mongodb://mongo:27017/registry"
        }
    },

    mailer: {
        transport: process.env.EMAIL_TRANSPORT || false,
        url_root: process.env.URL_ROOT || 'http://localhost:18000/',
        password_reset_path: process.env.PASSWORD_RESET_PATH || '/reset',
        emails: {
            from: process.env.EMAIL_FROM || 'All Binary <support@allbinary.se>',
            lang: {
                en: {
                    request_password_reset: {
                        subject: 'Password reset request',
                        html: './emails/en/request_password_reset.html',
                        text: './emails/en/request_password_reset.txt'
                    },
                    password_reset: {
                        subject: 'Password reset',
                        html: './emails/en/password_reset.html',
                        text: './emails/en/password_reset.txt'
                    }
                },
                sv: {
                    request_password_reset: {
                        subject: 'Begäran om återställning av lösenord',
                        html: './emails/sv/request_password_reset.html',
                        text: './emails/sv/request_password_reset.txt'
                    },
                    password_reset: {
                        subject: 'Återställning av lösenord',
                        html: './emails/sv/password_reset.html',
                        text: './emails/sv/password_reset.txt'
                    }
                }
            }
        }
    },

    users: {
        // bcrypt salt length for passwords
        salt_length: 10
    },

    auth: {
        basic: {
            enabled: true,
            realm: process.env.AUTH_REALM || 'The Realm'
        }
    },

    jwt: {
        life: process.env.JWT_LIFE || 60000,
        options: {
            claims: {
                // name is always added
                iat: true,
                iss: process.env.JWT_ISS || false
            },

            crypto: {
                algorithm: process.env.JWT_ALGO || 'HS256',
                secret: process.env.JWT_SECRET || false
            }
        }
    },

    CORS: process.env.CORS_CONFIG ? JSON.parse(process.env.CORS_CONFIG) : { credentials: true,
                                                                            origins: ['*'],
                                                                            headers: ['x-requested-with']
                                                                          },

    server: {
	    ip: process.env.HTTPS_IP || '127.0.0.1',
        port: process.env.HTTPS_PORT || 443,
        name: 'jwt-token-provider',
        version: JSON.parse(fs.readFileSync('./package.json')).version,
        timeout: process.env.SERVER_TIMEOUT || 60000,
        certificate: process.env.SSL_CERTIFICATE ? fs.readFileSync(process.env.SSL_CERTIFICATE).toString() : null,
        key: process.env.SSL_KEY ? fs.readFileSync(process.env.SSL_KEY).toString() : null,
        ca: process.env.SSL_CA ? fs.readFileSync(process.env.SSL_CA).toString() : null
    },

    log: {
        name: 'jwt-token-provider',
        level: process.env.LOG_LEVEL || 'info'
    }
};

