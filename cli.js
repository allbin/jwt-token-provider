"use strict";

var config = require('./config');
var bunyan = require('bunyan');
var restify = require('restify');

var log = bunyan.createLogger({
    name: 'jwt-token-provider-cli',
    level: process.env.LOG_LEVEL || 'info'
});
var users = require('./user')(config, log);
var claims = require('./claim')(config, log);

function usage() {
    console.error("Usage:  " + process.argv[1]);
    console.error("\t--add-user username password fullname email [lang org]- Add user with password, email address and optional language and organization");
    console.error("\t--list-users - List users");
    console.error("\t--change-password username password - Change password for user");
    console.error("\t--delete-user username - Delete user (and all access)");
    console.error("\t--change-email username email - Change email for user");
    console.error("\t--change-language username language - Change language for user. Example: 'en' or 'sv'");
    console.error("\t--change-fullname username fullname - Change full name for user");
    console.error("\t--change-org - Change organization for user");
    console.error("\t--add-claim username service name value - Add a claim for a service. Use service '*' for all services");
    console.error("\t--add-claims username service claims - Add several claims with intact types. 'claims' is a json-object");
    console.error("\t--list-claims username [service] - List all claims for a user");
    console.error("\t--revoke-claim username service name - Revoke claim ");

    process.exit(1);
}

function iferror(err) {
    if(err) {
        log.error('failed', err);
        console.error('failed', err);
        process.exit(2);
    }
}

function results(message, res) {
    res = res && res.result ? res.result : res;
    if(res) {
        log.info(message, res);
        console.log(message);
        console.log(res);

        process.exit(0);
    } else {
        log.info(message + ' failed');
        console.log(message + ' failed');
        process.exit(1);
    }
}

console.log("process.argv.length", process.argv.length);
if(process.argv.length <= 2) {
    usage();
}

var arg1 = process.argv[2];

switch(arg1) {

// --add-user username password - Add user with password
case '--add-user':
    if(process.argv.length < 6) {
        process.exit(2);
    }
    users.add(process.argv[3], process.argv[4], process.argv[5], process.argv[6], process.argv[7], process.argv[8]).then(function(res) {
        results('create user', res);
    }).catch(function(err) {
        iferror(err);
    });
    break;

// --list-users - List users
case '--list-users':
    users.list().then(function(users) {
        results('list users', users);
    }).catch(function(err) {
        iferror(err);
    });
    break;
    
//  --change-password username password - Change password for user
case '--change-password':
    if(process.argv.length < 5) {
        process.exit(2);
    }
    users.set_password(process.argv[3], process.argv[4]).then(function(res) {
        iferror(res !== true ? new Error('update failed') : null);
        results('update password for ' + process.argv[3], res);
        process.exit(0);
    }).catch(function(err) {
        iferror(err);
    });
    break;

case '--change-fullname':
    if (process.argv.length < 5) {
        process.exit(2);
    }
    users.set_fullname(process.argv[3], process.argv[4]).then(function(res) {
        iferror(res !== true ? new Error('update failed') : null);
        results('update fullname for ' + process.argv[3], res);
        process.exit(0);
    }).catch(function(err) {
        iferror(err);
    });
    break;
case '--change-email':
    if (process.argv.length < 5) {
        process.exit(2);
    }
    users.set_email(process.argv[3], process.argv[4]).then(function(res) {
        iferror(res !== true ? new Error('update failed') : null);
        results('update email for ' + process.argv[3], res);
        process.exit(0);
    }).catch(function(err) {
        iferror(err);
    });
    break;
case '--change-language': {
    if (process.argv.length < 5) {
        process.exit(2);
    }
    users.set_language(process.argv[3], process.argv[4]).then(function(res) {
        iferror(res !== true ? new Error('update failed') : null);
        results('update language for ' + process.argv[3], res);
        process.exit(0);
    }).catch(function(err) {
        iferror(err);
    });
    break;
}
case '--change-org': {
    if (process.argv.length < 5) {
        process.exit(2);
    }
    users.set_org(process.argv[3], process.argv[4]).then(function(res) {
        iferror(res !== true ? new Error('update failed') : null);
        results('update org for ' + process.argv[3], res);
        process.exit(0);
    }).catch(function(err) {
        iferror(err);
    });
    break;
}
//    --delete-user username - Delete user (and all access)
case '--delete-user':
        if(process.argv.length < 4) {
            usage();
        }
        users.find_by_name(process.argv[3]).then(function(user) {
            if(!user) {
                iferror(new Error("No such user"));
            }

            claims.remove_all_claims_for_user(user._id).then(function() {
                users.remove_by_name(user.name).then(function(res) {
                    results('delete user', res);
                }).catch(function(err) {
                    iferror(err);
                });

            }).catch(function(err) {
                iferror(err);
            });

        }).catch(function(err) {
            iferror(err);
        });
        break;

//--add-claim username service name value - Add a claim for a service. Use service '*' for all services
case '--add-claim': // username service name value
    if(process.argv.length < 7) {
        usage();
    }

    users.find_by_name(process.argv[3]).then(function(user) {
        if(!user) {
            return iferror(new Error("No such user"));
        }
        claims.add_claim(user._id, process.argv[4], process.argv[5], process.argv[6]).then(function(res) {
            results('add claim service=' + process.argv[4] + " " + process.argv[5] + "=" + process.argv[6] + " for " + user.name, res);
	}).catch(function(err) {
            iferror(err);
	});
    }).catch(function(err) {
        iferror(err);
    });
    break;

case '--add-claims':
    if (process.argv.length < 6) {
        usage();
    }

    users.find_by_name(process.argv[3]).then(function(user) {
        if(!user) {
            return iferror(new Error("No such user"));
        }
        claims.add_claims(user._id, process.argv[4], JSON.parse(process.argv[5])).then(function(res) {
            results('add claims service=' + process.argv[4] + "=" + process.argv[5] + " for " + user.name, res);
        }).catch(function(err) {
            iferror(err);
        });
    }).catch(function(err) {
        iferror(err);
    });
    break;

// --list-claims username [service] - List all claims for a user
case '--list-claims': // username [service]
    users.find_by_name(process.argv[3]).then(function(user) {
        if(!user) {
            return iferror(new Error("No such user"));
        }
        claims.find_for_user(user._id).then(function(claims) {
            results('list claims', claims.filter(function(c) {
                return (process.argv.length < 5) || (c.service === process.argv[4]);
            }));
	}).catch(function(err) {
            iferror(err);
	});

    }).catch(function(err) {
        iferror(err);
    });
    break;

// --revoke-claim username service name - Revoke claim
case '--revoke-claim':// username service claim_name
    if(process.argv.length < 6) {
        usage();
    }

    users.find_by_name(process.argv[3]).then(function(user) {
        if(!user) {
            return iferror(new Error("No such user"));
        }

        claims.remove_claim(user._id, process.argv[4],  process.argv[5]).then(function(res) {
            results('remove claim', res);
	}).catch(function(err) {
            iferror(err);
	});

    }).catch(function(err) {
        iferror(err);
    });
    break;

default:
    console.error('invalid argument',arg1);
    process.exit(1);
    break;
}
