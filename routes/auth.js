"use strict";
/**
   This file is part of jwt-token-provider.

   jwt-token-provider is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   jwt-token-provider is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with jwt-token-provider.  If not, see <http://www.gnu.org/licenses/>.
*/

module.exports = function(server, config, log) {
    var JWT = require('jwt-async');
    var jwt = new JWT(config.jwt.options);

    var users = require('../user')(config, log);
    var claims = require('../claim')(config, log);

    var Mailer = require('../mailer');
    var mailer = new Mailer(config.mailer);

    var basic_auth_enabled = config.auth.basic.enabled || true;
    var basic_auth_realm = config.auth.basic.realm || true;

    function require_auth(res) {
        if(basic_auth_enabled) {
            res.setHeader('www-authenticate', 'Basic realm="'+basic_auth_realm+'"');
        }
        res.send(401, 'Please provide authentication credentials');
    }

    // Given a login, send an email with a pw-reset code
    server.post('/request_reset_password', function(req, res, next) {
        if(!req.body.Login) {
            res.json(400, 'No Login Specified ' + req.body.Login);
            return next();
        }
        users.create_password_reset_code(req.body.Login).then(function(user) {
            console.log('Password reset code created', user);

            try {
                mailer.sendTemplateTo('request_password_reset', user.email, user.lang || "en", {
                    Code: user.pw_reset_code,
                    Login: user.name,
                    Link: config.mailer.url_root + config.mailer.password_reset_path + '?username=' + user.name + '&code=' + user.code
                });

                res.json(200, 'OK');
                return next();
            } catch(e) {
                res.json(500, e.message + '\n' + e.stack);
                return next();
            }
        }).catch(function(err) {
            if (err.message === "User not found") {
                res.json(404, 'User not found');
            } else {
                res.json(500, err.message + '\n' + err.stack);
            }
            return next();
        });
    });

    // Given a Login, a Code and a new Password and Password2, re-set the password
    server.post('/reset_password', function(req, res, next) {
        if(!req.body.Login) {
            res.json(400, 'No Login Specified');
            return next();
        }
        if(!req.body.Code) {
            res.json(400, 'No Code Specified');
            return next();
        }
        if(!req.body.Password) {
            res.json(400, 'No Password Specified');
            return next();
        }
        if(!req.body.Password2) {
            res.json(400, 'No Password2 Specified');
            return next();
        }
        if(req.body.Password !== req.body.Password2) {
            res.json(400, 'Password mismatch');
            return next();
        }

        users.verify_password_reset_code(req.body.Login, req.body.Code).then(function(user) {
            users.set_password(req.body.Login, req.body.Password).then(function() {
                mailer.sendTemplateTo('password_reset', user.email, user.lang || "en", { Login: user.name });
                res.json(200, 'OK');
                return next();
            }).catch(function(err) {
                res.json(500, 'Failed to set new password: ' + err.message);
                return next();
            });
        }).catch(function(/* err */) {
            res.json(400, 'Password reset code mismatch');
        });
    });

    // Given a valid JWT token, issues a new token with updated expiry
    server.get('/renew', function(req, res, next) {

        if(!req.authorization || req.authorization.scheme !== 'Bearer') {
            require_auth(res);
            return next();
        }

        var token = req.authorization.credentials;
        jwt.verify(token, function(err, data) {

            var claims = data.claims;
            var now = Date.now()/1e3;

            // If the token has expired, there's nothing we can do..

            if(claims.exp < now) { // Expired?
                res.json(401, 'Not authorized');
                return next();
            }

            // If the token expires too far in the future, there is no reason to renew it.

            if (claims.exp - now >  parseInt(config.jwt.life, 10) /2) {
                res.json(400, 'Bad Request: Expiry is too far in the future for renewal. ' +
                         'Wait until at least half the validity has passed before renewing.');
                return next();
            }

            users.get_by_id(claims.uid).then(function(user) {
                createToken(user, claims.svc, claims.scope).then(function(data) {
                    res.json(200, {token: data});
                    return next();
                }).catch(function(e) {
                    log.warn('Failed to create token', e, e.stack);
                    return next(new Error('Failed to create token'));
                });
            }).catch(function(e) {
                log.warn('Failed to look up user', e, e.stack);
                return next(new Error('Failed to look up user'));
            });
        });
    });

    var createToken = function(user, service, scope) {
        return new Promise(function(resolve, reject) {
            // authed user!
            claims.find_for_user(user._id).then(function(user_claims) {
                log.info('user claims', user_claims);
                var claims = {
                    name: user.name,
                    full_name: user.full_name,
                    email: user.email,
                    org: user.org,
                    lang: user.lang || "en",
                    uid: user._id,
                    nbf: (Math.floor(Date.now()/1000) - parseInt(config.jwt.life, 10)), // allow for some clock drift
                    exp: (Math.floor(Date.now()/1000) + parseInt(config.jwt.life, 10)),
                    svc: service,
                    scp: scope
                };

                user_claims.filter(function _service_filter(uc) {
                        return uc.service === '*' || uc.service === service;
                }).filter(function(uc) {
                        claims[uc.name] = uc.value;
                });
                log.trace('all claims', claims);

                try {
                    jwt.sign(claims, function(err, data) {
                        if(err) {
                            log.warn('jwt sign error', err, err.stack);
                            return reject(new Error('jwt sign error: ' + err));
                        }
                        log.trace('signed', err, data);
                        resolve(data);
                    });
                } catch(e) {
                    reject(e);
                }
            });
        });
    };

    // Given query parameter service and basic Authorization, returns a JWT token
    server.get('/token', function(req, res, next) {
        var service = req.query.service;
        if(!service) {
            res.send(400, 'Must provide service');
            return next();
        }
        var scope = req.query.scope || ''; // currently ignored
        var scheme = req.authorization ? req.authorization.scheme.toLowerCase() : null;

        if (!(req.authorization && (req.authorization.basic || scheme === 'bearer'))) {
            require_auth(res);
            return next();
        }

        if (scheme === 'bearer') {
            jwt.verify(req.authorization.credentials, function(err, obj) {
                if (err) {
                    return next(err);
                }

                var now = Date.now() / 1000;
                if (obj.claims.exp <= now || (obj.claims.nbf ? obj.claims.nbf > now : false)) {
                    return next(new Error('provided token was outside its validity window'));
                }

                users.get_by_id(obj.claims.uid).then(function(user) {
                    createToken(user, service, scope).then(function(data) {
                        res.json(200, {token: data});
                        return next();
                    });
                }).catch(function(err) {
                    return next(err);
                });
            });
            return;
        }

        var username = req.authorization.basic.username;
        var password = req.authorization.basic.password;
        log.info('username,password', [username, password]);
        users.verify_password(username, password).then(function(user) {
            if(!user) {
                log.warn('get.token:auth failed', username);
                require_auth(res);
                return next();
            }
            log.info('authed user', user);
            createToken(user, service, scope).then(function(data) {
                res.json(200, {token: data});
                return next();
            }).catch(function(err) {
                return next(new Error('Internal Error', err));
            });
        }).catch(function(/* err */) {
            require_auth(res);
            return next();
        });
    });
};
