# JWT token provider

Simplistic mongodb-backed JWT token provider with SSL support that handles users and claims. Given proper
credentials, it returns a JWT token with per-user defined claims.

It includes both a HTTP REST-API for generating tokens, a command line utility for managing users and claims,
and a Docker image build file for easy cloud-deployment.


## Quickstart

### Try the prebuilt Docker image

https://hub.docker.com/r/allbinarian/jwt-token-provider/


### From source

#### Clone the repository

    git clone https://bitbucket.org/mrorigo/jwt-token-provider.git

#### Run from the shell

    npm install
    export MONGO_URL=mongodb://127.0.0.1/jwt-provider
    export HTTPS_PORT=1443
    export EMAIL_TRANSPORT=
    export JWT_SECRET=secret
    export JWT_ISS=test-iss
    node cli.js --add-user user1 password fullname email@example.com
    node cli.js --add-claim user1 admin true
    npm start


#### Run in a Docker container

    docker build -t jwt-token-provider .
    CMD="docker run \
      -e MONGO_URL=mongodb://127.0.0.1/jwt-provider \
      -e HTTPS_PORT=1443 \
      -e JWT_SECRET=secret \
      -e JWT_ISS=test-iss \
      -p 1443:1443 \
      -it --rm jwt-token-provider"
    $CMD /cli --add-user user1 password fullname email@example.com
    $CMD /cli --add-claim user1 admin true

### Try it out

Use curl to fetch a new token for the user created above.
    
    curl -u user1:password https://127.0.0.1:1433/token
    {"token":"eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJuYW1lIjoidXNlcjEiLCJhZG1pbiI6InRydWUifQ.6dyQjCVb7lYCOf6bj57n1wqT7GK8XlttNquwESEzd56ioXXjTPuYnDB7COTMaTg5UgpBcUpPeXDjEcWURzCM2Q"}

Paste this token into jwt.io along with the secret above (secret), and verify the token!


## Configuration

Configuration is defined in config.js with reasonable defaults, and are overideable using the following environment variables:

    Name                Default value                           Description
    MONGO_URL           mongodb://127.0.0.1/jwt-provider        The mongo URI to use
    HTTPS_IP            127.0.0.1                               The IP to bind to
    HTTPS_PORT          443                                     HTTP(s) port to listen to on HTTPS_IP
    SSL_CERTIFICATE                                             File name of a SSL certificate file
    SSL_KEY                                                     File name of a SSL key file
    SSL_CA                                                      File name of the SSL CA file
    LOG_LEVEL           info                                    Logging level (error,warn,info,trace,debug)
    AUTH_REALM          The Realm                               Auth realm to use in www-authenticate response header
    SERVER_TIMEOUT      60000                                   Global timeout value for Restify
    EMAIL_TRANSPORT                                             nodemailer transport uri (see http://nodemailer.org/)
    EMAIL_FROM          'All Binary <support@allbinary.se>'     From-address for (password reset) emails
    URL_ROOT            'http://localhost:18000/'               The URL to use for links in emails
    JWT_ALGO            HS256                                   The signature algorithm to use (ex HS512)
    JWT_SECRET          secret                                  The global JWT secret
    JWT_ISS             test-iss                                The JWT issuer set in the 'iss' claim of tokens
    JWT_LIFE            60000                                   The lifetime of a issued JWT token ('iat' claim + JWT_LIFE = 'exp' claim)
    CORS_CONFIG         {credentials:true,origins: ['*'],       CORS config, see @restify.CORS
                         headers: ['x-requested-with']}

## Default claims

All tokens include a set of Registered Claim Names from the specification, namely;

    iss    -       Taken from JWT_ISS
    iat    -       NumericDate value
    exp    -       NumericDate value, iat + JWT_LIFE 
    nbf    -       NumericDate value, iat - JWT_LIFE 

The tokens also include claims for a 'name' and a 'uid', both strings, where the latter is a glbally unique identifier and the former is the login name of the user.


## Manage users and claims

To manage the contents if the database, use the `cli` command line utility. Executing the script with a `-h` argument displays it's usage:

	`--add-user username password fullname email` - Add user with password, full name and email
	`--list-users` - List users
	`--change-password` username password - Change password for user
    `--change-email` username email - Change email address for user
    `--change-fullname` username fullname - Change full name for user
	`--delete-user` username - Delete user (and all access)

	`--add-claim` username service name value - Add a claim for a service. Use service '*' for all services
    `--add-claims` username service claims - Adds multiple claims for a service. 'claims' is a json object.
	`--list-claims` username [service] - List all claims for a user
	`--revoke-claim` username service name - Revoke claim 


## Password reset process

The user password reset process includes first generating a unique password-reset-token, sending this link via email to the user, and the user clicking (or copying) this link, where they are presented with a form to specify a new password.

For this to work properly, the username must be an email-address. If you wish to use a different username to display in your application, it is suggested that you add a claim on the user for that (perhaps 'profile_name' or 'display_name' would be suitable).

You also need to configure how to send outgoing email, using the EMAIL_TRANSPORT environment variable. For a full description on how to construct a proper transport url, see the [nodemailer](https://nodemailer.org) documentation.

The contents of the two emails sent from the system are specified using two template files each, one text- and one html-version, both found in the 'emails' sub-folder.
