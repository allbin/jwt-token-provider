"use strict";
/**
   This file is part of jwt-token-provider.

   jwt-token-provider is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   jwt-token-provider is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with jwt-token-provider.  If not, see <http://www.gnu.org/licenses/>.
*/
module.exports = function(config) {

    var bcrypt = require('bcrypt');
    var mongo = require('mongoskin');
    var db = mongo.db(config.db.mongo.url, {native_parser:true});
    var AutoInc = require('./autoinc');
    var seq = new AutoInc(db, 'users');

    db.bind('users').bind({
        add: function(name, password, fullname, email, lang, org) {
            return new Promise(function(resolve, reject) {
                seq.getNextSequence().then((seq) => {
                    bcrypt.hash(password, config.users.salt_length, function(err, hash) {
                        let now = Date.now();
                        let new_user = {
                            _id: seq,
                            name: name,
                            password: hash,
                            full_name: fullname,
                            email: email,
                            last_modified: now,
                            created_at: now
                        };
                        if (lang) {
                            new_user.lang = lang;
                        }
                        if (org) {
                            new_user.org = org;
                        }

                        this.insert(new_user, function(err, res) {
                            if (err) {
                                return reject(err);
                            }
                            return resolve(res);
                        });
                    }.bind(this));
                }).catch((err) => {
                    return reject(err);
                });
            }.bind(this));
        },

        list: function() {
            return new Promise(function(resolve, reject) {
                this.find({deleted: null}).toArray(function(err, list) {
                    if(err) {
                        return reject(err);
                    }
                    resolve(list);
                });
            }.bind(this));
        },

        remove_by_name: function(name) {
            return new Promise(function(resolve, reject) {
                this.remove({name: name}, {justOne: true}, function(err, res) {
                    if(err) {
                        return reject(err);
                    }
                    resolve(res);
                });
            }.bind(this));
        },

        get_by_id: function(id) {
            return new Promise(function(resolve, reject) {
                this.findOne({_id: id, deleted: null}, function(err, user) {
                    if(err) {
                        return reject(err);
                    }
                    return user ? resolve(user) : reject(new Error('Not found'));
                });
            }.bind(this));
        },

        find_by_name: function(name) {
            return new Promise(function(resolve, reject) {
                this.findOne({name: name, deleted: null}, function(err, user) {
                    if(err) {
                        return reject(err);
                    }
                    resolve(user);
                });
            }.bind(this));
        },

        verify_password_reset_code: function(name, code) {
            return new Promise(function(resolve, reject) {
                this.find_by_name(name).then(function(user)  {
                    if(!user) {
                        return reject(new Error('no such user'));
                    }

                    console.log("compare reset code for user " + name + " (" + code + " vs " + user.pw_reset_code + ")");

                    bcrypt.compare(code, user.pw_reset_code, function(err, res) {
                        if(err) {
                            return reject(new Error('bcrypt.compare error', err));
                        }
                        if(res !== true) {
                            return reject(new Error('Invalid code'));
                        }
                        return resolve(user);
                    });
                }).catch(function(err) {
                    return reject(new Error('error in verify_password_reset_code/find_by_name', err));
                });
            }.bind(this));
        },

        create_password_reset_code: function(name) {
            return new Promise(function(resolve, reject) {
                this.find_by_name(name)
                .then(function(user) {
                    if(!user) {
                        return reject(new Error('User not found'));
                    }
                    var code = Date.now() + user._id + Math.floor(Math.random() * 2147483647);

                    bcrypt.hash(code.toString(), 8, function(err, hash) {
                        if (err) {
                            return reject(err);
                        }
                        this.update({_id: user._id}, {$set: {pw_reset_code: hash}}, function(err) {
                            if(err) {
                                return reject(err);
                            }
                            user.code = code.toString();
                            user.pw_reset_code = hash;
                            resolve(user);
                        });
                    }.bind(this));
                }.bind(this))
                .catch(function(err) {
                    return reject(err);
                });
            }.bind(this));
        },

        set_password: function(name, password) {
            return new Promise(function(resolve, reject) {
                console.log("setting new password for user " + name);
                this.find_by_name(name)
                .then(function(user) {
                    if(!user) {
                        return reject(new Error('user not found'));
                    }

                    bcrypt.hash(password, config.users.salt_length, function(err, hash) {
                        let now = Date.now();
                        this.update({_id: user._id}, {$set: {pw_reset_code: false, password: hash, last_modified: now}}, function(err) {
                            if(err) {
                                return reject(err);
                            }
                            return resolve(true);
                        });
                    }.bind(this));
                }.bind(this))
                .catch(function(err) {
                    return reject(err);
                });
            }.bind(this));
        },

        verify_password: function(name, password) {
            return new Promise(function(resolve, reject) {
                this.find_by_name(name).then(function(user)  {
                    if(!user) {
                        return reject(new Error('no such user'));
                    }

                    bcrypt.compare(password, user.password, function(err, res) {
                        if(err) {
                            return reject(new Error('bcrypt.compare error', err));
                        }
                        if(res !== true) {
                            return reject(new Error('Invalid password'));
                        }
                        return resolve(user);
                    });
                }).catch(function(err) {
                    return reject(new Error('error in verify_password/find_by_name', err));
                });
            }.bind(this));
        },

        set_fullname: function(name, fullname) {
            return new Promise(function(resolve, reject) {
                this.find_by_name(name).then(function(user) {
                    let now = Date.now();
                    if (!user) {
                        return reject(new Error('user not found'));
                    }
                    this.update({_id: user._id}, {$set: {full_name: fullname, last_modified: now}}, function(err) {
                        if (err) {
                            return reject(err);
                        }
                        return resolve(true);
                    }.bind(this));
                }.bind(this)).catch(function(err) {
                    return reject(new Error('error in set_fullname/find_by_name', err));
                });
            }.bind(this));
        },

        set_email: function(name, email) {
            return new Promise(function(resolve, reject) {
                this.find_by_name(name).then(function(user) {
                    let now = Date.now();
                    if (!user) {
                        return reject(new Error('user not found'));
                    }
                    this.update({_id: user._id}, {$set: {email: email, last_modified: now}}, function(err) {
                        if (err) {
                            return reject(err);
                        }
                        return resolve(true);
                    }.bind(this));
                }.bind(this)).catch(function(err) {
                    return reject(new Error('error in set_email/find_by_name', err));
                });
            }.bind(this));
        },

        set_language: function(name, lang) {
            return new Promise(function(resolve, reject) {
                this.find_by_name(name).then(function(user) {
                    let now = Date.now();
                    if (!user) {
                        return reject(new Error('user not found'));
                    }
                    this.update({_id: user._id}, {$set: {lang: lang, last_modified: now}}, function(err) {
                        if (err) {
                            return reject(err);
                        }
                        return resolve(true);
                    }.bind(this));
                }.bind(this)).catch(function(err) {
                    return reject(new Error('error in set_language/find_by_name', err));
                }.bind(this));
            }.bind(this));
        },

        set_org: function(name, org) {
            return new Promise(function(resolve, reject) {
                this.find_by_name(name).then(function(user) {
                    let now = Date.now();
                    if (!user) {
                        return reject(new Error('user not found'));
                    }
                    this.update({_id: user._id}, {$set: {org: org, last_modified: now}}, function(err) {
                        if (err) {
                            return reject(err);
                        }
                        return resolve(true);
                    }.bind(this));
                }.bind(this)).catch(function(err) {
                    return reject(new Error('error in set_org/find_by_name', err));
                }.bind(this));
            }.bind(this));
        }

    });
    db.users.ensureIndex( { "name": 1 }, {unique: true} );

    return db.users;
};
