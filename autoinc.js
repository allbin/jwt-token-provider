var AutoInc = function(db, sequenceName) {

    this.db = db;
    this.collection = this.db.collection('counters');

    this.sequenceName = sequenceName;

    this.getNextSequence = function() {
        return new Promise((resolve, reject) => {
            this.collection.findAndModify(
                {_id: this.sequenceName},
                null,
                { $inc: { seq: 1 } },
                { upsert: true, new: true },
            (err, result) => {
                if (err) {
                    return reject(err);
                }
                return resolve(result.value.seq);
            });
        });
    };
};

module.exports = AutoInc;
