"use strict";
/**
   This file is part of jwt-token-provider.

   jwt-token-provider is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   jwt-token-provider is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with jwt-token-provider.  If not, see <http://www.gnu.org/licenses/>.
*/

var config = require('./config');
var bunyan = require('bunyan');
var restify = require('restify');

var log = config.server.log = bunyan.createLogger(config.log);

log.info("config", config);

var server = restify.createServer(config.server);

server.use(restify.acceptParser(server.acceptable));
server.use(restify.authorizationParser());
server.use(restify.dateParser());
server.use(restify.queryParser({mapParams: false}));
server.use(restify.bodyParser());
if(config.debug) {
    server.use(restify.fullResponse());         // sets up all of the default headers for the system
}

server.on('uncaughtException', function _uncaught_exception(req, res, obj, err) {
    log.error('uncaughtException', obj, err);
    res.json(500, {message: "Internal server error"});
    res.end();
});


server.use(function(req,res,next) {
    log.info("Request", [req.url, req.body, req.query]);
    return next();
});

server.use(restify.CORS(config.CORS));

server.opts(/.*/, function (req,res,next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Methods", req.header("Access-Control-Request-Method"));
    res.header("Access-Control-Allow-Headers", req.header("Access-Control-Request-Headers"));
    res.send(200);
    return next();
});

require('./routes/auth.js')(server, config, log);

server.listen(config.server.port, config.server.ip, function(err) {
    if(err) {
        log.error("Failed to start listening on port " + config.server.port, err);
        process.exit(2);
    }
    log.info("%s listening at %s", server.name, config.server.port);
});
