"use strict";

module.exports = function(config, log) {

    var mongo = require('mongoskin');
    var db = mongo.db(config.db.mongo.url, {native_parser:true});

    db.bind('claims').bind({

        find_for_user: function(user_id) {
            return new Promise(function(resolve, reject) {
                return this.find({user_id: user_id}).toArray(function(err, claims) {
                    if(err) {
                        return reject(err);
                    }
                    resolve(claims);
                });
            }.bind(this));
        },

        add_claims: function(user_id, service, claims) {
            console.log(claims);
            return new Promise(function(resolve, reject) {
                let keys = Object.keys(claims);
                let promises = keys.map(function(key) {
                    let name = key;
                    let value = claims[key];
                    return this.add_claim(user_id, service, name, value);
                }.bind(this));

                Promise.all(promises)
                    .then(function(res) {
                        return resolve(res);
                }.bind(this))
                    .catch(function(err) {
                        return reject(err);
                    });
            }.bind(this));
        },

        add_claim: function(user_id, service, name, value) {
            return new Promise(function(resolve, reject) {
                this.insert({
                    user_id: user_id,
                    service: service,
                    name: name,
                    value: value
                }, function(err, res) {
                    if(err) {
                        return reject(err);
                    }
                    resolve(res);
                });
            }.bind(this));
        },

        remove_claim: function(user_id, service, name) {
            return new Promise(function(resolve, reject) {
                var q = {user_id: user_id, service: service, name: name};
                log.trace('claims.remove_claim query', q);
                this.remove(q, function(err, res) {
                    if(err) {
                        return reject(err);
                    }
                    resolve(res);
                });
            }.bind(this));
        },

        remove_all_claims_for_user: function(user_id) {
            return new Promise(function(resolve, reject) {
                var q = {user_id: user_id};
                log.trace('claims.remove_all_claims_for_user query', q);
                this.remove(q, function(err, res) {
                    if (err) {
                        return reject(err);
                    }
                    return resolve(res);
                });
            }.bind(this));
        }
    });

    return db.claims;
};
